odoo.define('better_checkbox_widget', function (require) {
    "use strict";

    var AbstractField = require('web.AbstractField');
    var fieldRegistry = require('web.field_registry');

    var core = require('web.core');
    var qweb = core.qweb;

    var BetterFieldMany2ManyCheckBoxes = AbstractField.extend({
        template: 'FieldMany2ManyCheckBoxes',
        events: _.extend({}, AbstractField.prototype.events, {
            change: '_onChange',
        }),
        specialData: "_fetchSpecialRelation",
        supportedFieldTypes: ['many2many'],

        /**
         * @public
         */
        init: function () {
            this._super.apply(this, arguments);
            this.m2mValues = this.record.specialData[this.name];
        },

        /**
         * @public
         */
        start: function() {
            this._renderCheckboxes();
        },

        /**
         * @public
         */
        isSet: function () {
            return true;
        },

        /**
         * @private
         */
        _renderCheckboxes: function () {
            var self = this;
            this.m2mValues = this.record.specialData[this.name];
            this.$el.html(qweb.render(this.template, {widget: this}));
            _.each(this.value.res_ids, function (id) {
                self.$('input[data-record-id="' + id + '"]').prop('checked', true);
            });
        },
        /**
         * @override
         * @private
         */
        _renderEdit: function () {
            this._renderCheckboxes();
        },
        /**
         * @override
         * @private
         */
        _renderReadonly: function () {
            this._renderCheckboxes();
            this.$("input").prop("disabled", true);
        },

        /**
         * @private
         */
        _onChange: function () {
            var ids = _.map(this.$('input:checked'), function (input) {
                return $(input).data("record-id");
            });
            this._setValue({
                operation: 'REPLACE_WITH',
                ids: ids,
            });
        },

    });

    fieldRegistry.add('better_checkbox_widget', BetterFieldMany2ManyCheckBoxes);

    return {
        BetterFieldMany2ManyCheckBoxes: BetterFieldMany2ManyCheckBoxes,
    };
});
