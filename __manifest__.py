# -*- coding: utf-8 -*-
{
    'name': 'CRM Checklist',
    'version': '12.0.1.0',
    'summary': """Checklist for CRM Leads""",
    'description': """Crm checklist""",
    'category': 'crm',
    'author': 'Sergiy',
    'website': "",
    'depends': ['base', 'crm', 'web'],
    'data': [
        'security/ir.model.access.csv',
        'views/crm_lead_view.xml',
        'views/crm_lead_checklist_settings.xml',
        'views/header.xml',
    ],
    'qweb': [
        "static/src/xml/widget_template.xml"
    ],
    'demo': [
        'demo/checklist_demo.xml'
    ],
    'license': 'AGPL-3',
    'installable': True,
    'auto_install': False,
    'application': False,
}
