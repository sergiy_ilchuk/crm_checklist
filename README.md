# Checklist

## Overview of approach taken.

Create a module,

Model calculates the amount of completed checks by percent and default odoo progressbar widget is used in all views to display the progress.
Added a border to the progress bar just for better visibility.
Edit views that are visible to the user in CRM: kanban, tree view and lead view.
Create settings in the top menu CRM/Configuration/Pipeline/Lead Checklist.
A new widget has to be done to allow saving the checklist state with AJAX and don't need to edit the whole form.
The widget uses the same template as FieldMany2ManyCheckBoxes and practically the same, but it gets explicitly rendered at start.
Add generic demo data for the checklist to be visible right away.

## Assumptions.

I cover all basic acceptance criteria, but the second screenshot in the ticket has the progress bar displayed
on a 'create' form, so I assume we dont need it here, but it was meant to be on the kanban ticket itself.
This module has to have all the basic features working, and can be refactored and streamlined further.

## Recommendations for future enhancements, or areas which can be improved.

1. Tree view progressbar could be styled more compact
2. Checklist can be created for user, or user editable for himself
