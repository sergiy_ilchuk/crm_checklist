# -*- coding: utf-8 -*-

from odoo import models, fields, api


class CrmChecklistLead(models.Model):
    _inherit = 'crm.lead'

    @api.depends('crm_checklist')
    def crm_checklist_progress(self):
        """:return the value for the check list progress"""
        for rec in self:
            total_len = self.env['crm.checklist'].search_count([])
            check_list_len = len(rec.crm_checklist)
            if total_len != 0:
                rec.crm_checklist_progress = (check_list_len*100) / total_len

    crm_checklist = fields.Many2many('crm.checklist', string='Crm Checkist')
    crm_checklist_progress = fields.Float(compute=crm_checklist_progress, string='Progress', store=True, recompute=True, default=0.0)


class CrmChecklist(models.Model):
    _name = 'crm.checklist'
    _description = 'Checklist for CRM lead'

    name = fields.Char(string='Name', required=True)
    description = fields.Char(string='Description')
